function [K, obj] = design_controller(model, K_init, order, W_LS, M_LS, options)
%DESIGN_CONTROLLER Design data-driven controller using loop shaping
% 
%   [K, OBJ] = DESIGN_CONTROLLER(model, K_init, order, W_LS, M_LS) design a 
%       controller of order `order` for robust stability with loop shaping as 
%       performance objective using `K_init` as initial controller. `W_LS` is
%       the weighing filter and `M_LS` is the desired closed-loop model
%       reference.
% 
%   [K, OBJ] = DESIGN_CONTROLLER(..., "MaxIter", max_iter) set the maximum
%       number of iterations allowed for controller design. 
% 
%   [K, OBJ] = DESIGN_CONTROLLER(..., "solver", solver) sets the solver to use 
%       for convex optimisation. Tested solvers are: Mosek (default) and SeDuMi.
% 
%   [K, OBJ] = DESIGN_CONTROLLER(..., "verbosity", level) sets the verbosity
%       level.
% 
%   ----------------------------------------------------------------------------
%   Output:
%       K   : Designed controller
%       obj : Achieved 2-norm objective
%       
%   ----------------------------------------------------------------------------
%   Verbosity Levels:
%       0: No output
%       1: Basic logs
%       2: Complete logs with optimisation logs
% 
%   ----------------------------------------------------------------------------
%   Description:
%       Uses the modified datadriven package to design controller for nominal
%       performance with robust stability.
% 
%       Performance objective is:
% 
%               min ║W_LS * (M_LS - T)║₂
% 
%       where, T is the complementary sensitivity function (from r to y).
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

    arguments
        model (1, 1) struct
        K_init (1, 1) lti
        order (1, 1) int32
        
        % Loop Shaping
        W_LS (1, 1) lti = tf()
        M_LS (1, 1) lti = tf()
        
        % Loop Opening
        options.UseLoopOpening (1, 1) logical = false
        
        % Integrator
        options.UseIntegrator (1, 1) logical = true
        
        options.MaxIter (1, 1) int32 = 1e2
        options.solver (1, 1) string = "mosek"
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
    end

    logger = @(type, varargin) utils.logger_verbose(options.verbosity, type, varargin{:});
    logger("START", mfilename);
    cleanUp = onCleanup(@() logger("STOP", mfilename));
    
    %% Initial Controller
    
    % Get numerator/denominator of PID controller
    [num, den] = tfdata(K_init, 'v'); 
    den(order + 1) = 0; % Zero padding
    num(order + 1) = 0; % Zero padding

    Fx = 1;
    Fy = 1;
    
    if options.UseIntegrator
        Fy = [1, -1];
        den = deconv(den, Fy);
    end

    if options.UseLoopOpening
        Fx = [1, 1];
        num = deconv(num, Fx);
    end

    %% Setup and solve problem in datadriven framework
    tic;
    
    logger("INFO", "Setting up the problem in datadriven framework...");
    
    % Load empty structure
    [SYS, OBJ, CON, PAR] = datadriven.utils.emptyStruct();

    % Assemble initial controller
    CTRL = struct('num', num, 'den', den, 'Ts', model.NominalModel.Ts, 'Fx', Fx, 'Fy', Fy); 

    % Set the nominal system
    SYS.controller = CTRL;
    SYS.model = model.NominalModel;
    SYS.W = model.UncertaintySet.Frequencies;

    % Set the loop-shaping objectives
    OBJ.loop_shaping.W_LS = W_LS;
    OBJ.loop_shaping.M = M_LS;

    % Set optimisation parametera
    PAR.tol = 1e-4;
    PAR.solver = options.solver;
    PAR.maxIter = options.MaxIter;
    PAR.robustEllipsoid = model.UncertaintySet.A;
    
    % Solve for optimal controller
    logger("INFO", "Solving for optimal controller in datadriven framework...");
    [K, obj] = datadriven.datadriven(SYS, OBJ, CON, PAR, options.verbosity==2);
    
    ellapsed_time = toc;
    logger("INFO", "Optimal controller found in %.4f s", ellapsed_time);
    
    %% Extract out the outputs
    K = datadriven.utils.toTF(K);
    obj = obj.H2;
end