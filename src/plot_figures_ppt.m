%% Plots for presentation
% 
%   Generate plots for paper from the simulation data.
% 
%   Loads `.mat` file geenrated during simulation.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

% Cleanup before starting
clearvars;
clear('yalmip');
close all;
clc;

%% Basic config
% Export Graphic Options (set [] for paper)
exportgraphics_opts = ["ContentType", "vector", "BackgroundColor", "none"];

%% Load files
% Load `.mat` file
load(fullfile("Data", "sim_models.mat"));

%% Plot `Nyquist` of FRFs and the nominal model for elliptical & disk uncertainty sets
z_frd_models = squeeze(freqresp(frd_models, w));

fig = figure();
tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile();
hold on; axis equal; box on; grid minor; grid on;

ytickformat('$%d$'); xtickformat('$%d$');
ax.TickLabelInterpreter = "latex";
ax.FontSize = 20;
ax.Box = "on";

for i = 1:10
plot(real(z_frd_models(:,i)), imag(z_frd_models(:,i)), ...
    "Color", [237/255, 177/255, 32/255, 0.5], ...
    "LineWidth", 3);
end

i_ws = [340, 220, 240];
text_x = [ 0.5, -2.5, 3.5];
text_y = [ 0,  3.5, 6.5];
mkstys = ["d", "o", "s", "h"];
mksize_meas = 8;
mksize = 10;
for idx = 1:length(i_ws)
    i_w = i_ws(idx);
    mksty = mkstys(idx);

    % Measurements
    plot(real(z_frd_models(i_w, :)), imag(z_frd_models(i_w, :)), ...
        "LineStyle", "none", ...
        "Marker", mksty, ...
        "MarkerSize", mksize_meas, ...
        "Color", [0,0,0], ...
        "MarkerFaceColor", [0,0,0]);
    
    % Frequency labels
    text(text_x(idx), text_y(idx), ...
        sprintf("$%.2f$ [rad/s]", w(i_w)), ...
        "FontSize", 20, ...
        "Interpreter", "latex");
end

xlabel(ax, "Real Axis", "Interpreter", "latex");
ylabel(ax, "Imaginary Axis", "Interpreter", "latex");

fig.Renderer = "painters";
fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "ppt_nyquist.pdf"), exportgraphics_opts{:});

%% PLOT IO data
N = 2 / Z.Ts;

u = Z.InputData(1:N);
y = Z.OutputData(1:N);
t = Z.Tstart:Z.Ts:Z.Ts*N;

fig = figure('Position', [0, 0, 800, 400]);
fig.Renderer = "painters";
fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

h_tiledlayout = tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile(); 
hold on; box on; grid minor; grid on;

ax.TickLabelInterpreter = "latex";
ax.FontSize = 20;
ax.Box = "on";

h_u = stairs(t, u, ...
    "LineWidth", 2, ...
    "Color", "#D95319");

stairs(t, y, ...
    "LineStyle", "-", ...
    "LineWidth", 1, ...
    "Color", "#77AC30");
h_y = stairs(0, 0, ...
    "LineStyle", "-", ...
    "LineWidth", 2, ...
    "Color", "#77AC30");

leg = legend([h_u, h_y], ...
    "Input Data", ...
    "Output Data", ...
    "Location", "northeast");
leg.Interpreter = "latex";
leg.FontSize = 20;

xlabel(ax, "Time [s]", "Interpreter", "latex");
ylabel(ax, "", "Interpreter", "latex");

exportgraphics(fig, fullfile("Plots", "ppt_IO_data.pdf"), exportgraphics_opts{:});

%% PLOT frequency data
z_frd_models = squeeze(freqresp(frd_models, w));

fig = figure('Position', [0, 0, 400, 400]);
tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile();
hold on; axis equal; box on; grid minor; grid on;

ytickformat('$%d$'); xtickformat('$%d$');
ax.TickLabelInterpreter = "latex";
ax.FontSize = 20;
ax.Box = "on";

colors = (jet(20));
i = 10;
h_avg = plot(real(z_frd_models(:,i)), imag(z_frd_models(:,i)), ...
    "Color", "#0072BD", ...
    "LineWidth", 3);

xlabel(ax, "Real Axis", "Interpreter", "latex");
ylabel(ax, "Imaginary Axis", "Interpreter", "latex");

fig.Renderer = "painters";
fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "ppt_freq_data.pdf"), exportgraphics_opts{:});