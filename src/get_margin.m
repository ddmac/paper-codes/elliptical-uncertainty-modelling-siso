function margin = get_margin(model, K, options)
%GET_MARGIN Compute the stability margin for the controller
% 
%   margin = GET_MARGIN(model, K) computes the satbility margin of the
%       controller `K` using the `model`.
% 
%   margin = GET_MARGIN(..., "solver", solver) sets the solver to use for convex
%       optimisation. Tested solvers are: Mosek (default) and SeDuMi.
% 
%   margin = GET_MARGIN(..., "verbosity", level) sets the verbosity level.
% 
%   ----------------------------------------------------------------------------
%   Output:
%       margin : Computed stability margin in IQC-framework
% 
%   ----------------------------------------------------------------------------
%   Verbosity Levels:
%       0: No output
%       1: Basic logs
%       2: Complete logs with optimisation logs
% 
%   ----------------------------------------------------------------------------
%   Description:
%       The stability margin under IQC description can be defined as the
%       smallest scaling of the uncertainty set where the feedback connection 
%       becomes unstable (Hai-rong et al., 2002). For the margin $\geq 1$, 
%       the feedback connection is stable for the uncertainty set.
% 
% 
%       References:
%       [1] D. Hai-rong, G. Zhi-yong, W. Jin-zhi, and H. Lin, “Stability margin 
%           of systems with mixed uncertainties under the IQC descriptions,” 
%           Appl Math Mech, vol. 23, no. 11, pp. 1274–1281, Nov. 2002, 
%           doi: 10.1007/BF02439458.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 
    arguments
        model (1, 1) struct
        K (1, 1) lti
        options.solver (1, 1) string = "mosek"
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
    end

    logger = @(type, varargin) utils.logger_verbose(options.verbosity, type, varargin{:});
    logger("START", mfilename);
    cleanUp = onCleanup(@() logger("STOP", mfilename));
    
    %% Optimisation for margin
    logger("INFO", "Computing stability margin...");
    w = model.NominalModel.Frequency;
    
    U = feedback(K, model.NominalModel);
    U_f = freqresp(U, w);
    
    eta = sdpvar(1);

    constraints = [eta >= 0];
    for i_w = 1:length(w)
        tmp = [ ...
            U_f(i_w), U_f(i_w);
            eye(2);
            ];
        
        J = diag([1, 1j]);
        tmp2 = model.UncertaintySet.A{i_w} * J';

        PI = [...
            eta        , zeros(1, 2)    ;
            zeros(2, 1), - tmp2' * tmp2 ;
            ];

        constraints = [constraints, tmp' * PI * tmp <= 0];
    end
    objective = -eta;

    opt = sdpsettings(...
        'solver', char(options.solver), ...
        'verbose', options.verbosity==2 ...
        ); 
    JOB = optimize(constraints, objective, opt);

    if JOB.problem ~= 0
        logger("ERROR", "Optimisation cannot be solved!");
    end
    
    %% Extract Margin
    margin = double(eta);
    logger("INFO", "Found stability margin is %.2f!", margin);

end