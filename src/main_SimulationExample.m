%% Generate random models and perform uncertainty set identification
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
script_sim;

%% Plot results and save them in `Plots` folder
if ~exist("Plots", "dir")
    mkdir("Plots");
end
plot_figures_sim;

%% Post Clean-up
clearvars; clear("yalmip");
close all; clc;