%% Script for controller synthesis (Experiment)
% 
%   Performs the following actions:
%   1. Uncertainty set identification (elliptical & disk) for the FRFs
%      obtained from the PRBS experiments.
%   2. Design a model reference controller for the identified nominal models and
%      robust to the respective uncertinty sets.
%   3. Compute the stability margins of the controllers.
% 
%   Loads `.bin` files generated during PRBS experiments and save a `.mat` file
%   for plot generation.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

% Cleanup before starting
clearvars;
clear('yalmip');
close all;
clc;

%% Basic config
Ts = 2e-3;      % [s]
Fs = 1 / Ts;    % [Hz]

solver = "mosek"; % One of: mosek or sedumi

%% Load PRBS data & generate FRFs
n_prbs = 13;    % PRBS register length
period = 2^n_prbs-1;

PRBS_files = dir("Data/PRBS_13/logs_*.bin");
n_FRF = length(PRBS_files);

frd_models = cell(n_FRF, 1);
for i_file = 1:n_FRF
    [y, ~, u, ~, anyRef, ~] = utils.read_binary_file(...
        fullfile(PRBS_files(i_file).folder, PRBS_files(i_file).name) ...
    );

    anyRef(find(anyRef, period*2)) = false; % ignore first 2 periods
    Z = detrend(iddata(y(anyRef, 1), u(anyRef, 1), Ts, "Period", period));
    frd_models{i_file} = frd(etfe(Z));
end
frd_models = cat(3, frd_models{:});

%% Uncertainty set identification
w = logspace(...
    log10(frd_models.Frequency(1)), ...
    log10(frd_models.Frequency(end)), ...
    500).';

model_elliptical = find_elliptical_uncertainty(frd_models, w, "solver", solver);
model_disk = find_disk_uncertainty(frd_models, w, "solver", solver);

%% Controller synthesis
s = tf('s');

% Weighing filter for loop shaping
W_LS = 1 + 10/s;

% Reference Model
tau = 1/10;
M_LS = 1/(s*tau + 1);

% Initial satbilising controller
K_init = c2d(0.1/s, Ts);

[K_elliptical, obj_elliptical] = design_controller(model_elliptical, K_init, 10, W_LS, M_LS, "solver", solver);
[K_disk, obj_disk] = design_controller(model_disk, K_init, 10, W_LS, M_LS, "solver", solver);

%% Compute stability margins
margin_elliptical = get_margin(model_elliptical, K_elliptical, "solver", solver);
margin_disk = get_margin(model_disk, K_disk, "solver", solver);

%% Save `.mat` file for plot generation
save(fullfile("Data", "exp_controller_synthesis.mat"))