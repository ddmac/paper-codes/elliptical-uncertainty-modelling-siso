%% Plots for paper (Simulation)
% 
%   Generate plots for paper from the simulation data.
% 
%   Loads `.mat` file geenrated during simulation.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

% Cleanup before starting
clearvars;
clear('yalmip');
close all;
clc;

%% Basic config
colors = struct(...
    "EllipticalSet", "#0072BD", ...
    "DiskSet", "#D95319", ...
    "AvgModel", "#77AC30", ...
    "FRFs", "#EDB120");

%% Load files

% Export Graphic Options (set [] for paper)
exportgraphics_opts = ["ContentType", "vector", "BackgroundColor", "none"];

% Load `.mat` file
load(fullfile("Data", "sim_models.mat"));

%% Plot `Nyquist` of FRFs and the nominal model for elliptical & disk uncertainty sets
i_ws = [230, 240, 250, 340];

z_frd_models = squeeze(freqresp(frd_models, w));
z_avg = mean(z_frd_models, 2);
z_elliptical = squeeze(freqresp(model_elliptical.NominalModel, w));
z_disk = squeeze(freqresp(model_disk.NominalModel, w));

fig = figure();
tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile();
hold on; axis equal; box on; grid minor; grid on;

ytickformat('$%d$'); xtickformat('$%d$');
ax.TickLabelInterpreter = "latex";
ax.FontSize = 14;
ax.Box = "on";

h_avg = plot(real(z_avg), imag(z_avg), ...
    "Color", colors.AvgModel, ...
    "LineWidth", 3);
h_elliptical = plot(real(z_elliptical), imag(z_elliptical), ...
    "Color", colors.EllipticalSet, ...
    "LineWidth", 3);
h_disk = plot(real(z_disk), imag(z_disk), ...
    "Color", colors.DiskSet, ...
    "LineWidth", 3);
h_meas = plot(0, 0, ...
    "Color", colors.FRFs, ...
    "LineWidth", 3);

mkstys = ["d", "o", "s", "h"];
mksize_meas = 8;
mksize = 10;
for idx = 1:length(i_ws)
    i_w = i_ws(idx);
    mksty = mkstys(idx);

    % Measurements
    plot(real(z_frd_models(i_w, :)), imag(z_frd_models(i_w, :)), ...
        "LineStyle", "none", ...
        "Marker", mksty, ...
        "MarkerSize", mksize_meas, ...
        "Color", colors.FRFs, ...
        "MarkerFaceColor", colors.FRFs);
    
    % Average Model
    plot(real(z_avg(i_w, :)), imag(z_avg(i_w, :)), ...
        "LineStyle", "none", ...
        "Marker", mksty, ...
        "MarkerSize", mksize_meas, ...
        "Color", colors.AvgModel, ...
        "MarkerFaceColor", colors.AvgModel);

    % Disk Uncertainty Set
    utils.plot_ellipse(...
        z_disk(i_w), model_disk.UncertaintySet.A{i_w},...
        "Marker", mksty, ...
        "MarkerSize", mksize, ...
        "Color", colors.DiskSet);

    % Elliptical Uncertainty Set
    utils.plot_ellipse(...
        z_elliptical(i_w), model_elliptical.UncertaintySet.A{i_w},...
        "Marker", mksty, ...
        "MarkerSize", mksize, ...
        "Color", colors.EllipticalSet);   
end

leg = legend([h_meas, h_avg, h_disk, h_elliptical], ...
    "Measurements ($G_i$)", ...
    "Average Model", ...
    "Hindi et al. (2002)", ...
    "Proposed Method", ...
    "Location", "northeastoutside");
leg.Interpreter = "latex";
leg.FontSize = 14;
leg.Box = "off";

xlabel(ax, "Real Axis", "Interpreter", "latex");
ylabel(ax, "Imaginary Axis", "Interpreter", "latex");

fig.Renderer = "painters";
fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "sim_comp_plot.pdf"), exportgraphics_opts{:});

%% Plot areas of elliptical & disk uncertainty sets
fig = figure('Position', [0, 0, 500, 250]);
h_tiledlayout = tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile(); 
hold on; box on; grid minor; grid on;

ax.XScale = 'log';
ax.YScale = 'log';
ax.TickLabelInterpreter = "latex";
ax.FontSize = 14;
ax.Box = "on";

h_elliptical = plot( ...
    model_elliptical.NominalModel.Frequency, ...
    model_elliptical.UncertaintySet.Area, ...
    "Color", colors.EllipticalSet, ...
    "LineWidth", 2);
h_disk = plot( ...
    model_disk.NominalModel.Frequency, ...
    model_disk.UncertaintySet.Area, ...
    "Color", colors.DiskSet, ...
    "LineWidth", 2);

xlim([1, Fs*pi]);

xlabel(ax, "Frequency [rad/s]", "Interpreter", "latex");
ylabel(ax, "Area of uncertainity", "Interpreter", "latex");

leg = legend([h_disk, h_elliptical], ...
    "Hindi et al. (2002)", ...
    "Proposed Method", ...
    "Location", "northeast");
leg.Interpreter = "latex";
leg.FontSize = 14;
leg.Box = "off";

fig.Renderer = "painters";
fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "sim_area_plot.pdf"), exportgraphics_opts{:});
