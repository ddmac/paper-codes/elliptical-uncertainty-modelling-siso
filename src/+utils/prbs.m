function u = prbs(n_register, n_periods, nu, u_init, options)
%PRBS Compute PRBS signal
% 
%   u = PRBS(n_register, n_periods) computes the PRBS with `n_register` and
%       `n_periods`.
%
%   u = PRBS(..., nu) computes the PRBS for `nu` inputs.
%
%   u = PRBS(..., nu, u_init) computes the PRBS for `nu` inputs with `u_init` as
%       initalising register. `u_init` must only contain 0 or 1 as elements.
% 
%   u = PRBS(..., "verbosity", level) sets the verbosity level.
% 
%   ----------------------------------------------------------------------------
%   Output:
%       margin : Computed stability margin in IQC-framework
% 
%   ----------------------------------------------------------------------------
%   Verbosity Levels:
%       0: No output
%       1: Basic logs
%       2: Complete logs with optimisation logs
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

    arguments
        n_register (1, 1) int32 {mustBePositive}
        n_periods (1, 1) int32
        nu (1, 1) int32 = 1
        u_init (:, :) double = []
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
    end

    if isempty(u_init)
        u_init = randi([0, 1], n_register, nu);
    end

    % Logger handle
    logger = @(type, varargin) utils.logger_verbose(options.verbosity, type, varargin{:});
    
    u = []; % In case of error!
    
    %% Taps for PRBS registers
    taps_list = hex2dec({ ...
        '0' ;       % 1 (NA) -> Not Supported (no taps set)
        '3' ;       % 2
        '6' ;       % 3
        'C' ;       % 4
        '14' ;      % 5
        '30' ;      % 6
        '60' ;      % 7
        'B8' ;      % 8
        '110' ;     % 9
        '240' ;     % 10
        '500' ;     % 11
        'E08' ;     % 12
        '1C80' ;    % 13
        '3802' ;    % 14
        '6000' ;    % 15
        'D008' ;    % 16
        '12000' ;   % 17
        '20400' ;   % 18
        '72000' ;   % 19
        '90000' ;   % 20
        %---- Unusable ---- (Too much memory consumption)
    %     0x140000 ;  % 21
    %     0x300000 ;  % 22
    %     0x420000 ;  % 23
    %     0xE10000 ;  % 24
    %     0x0 ;       % 25 (NA)
    %     0x0 ;       % 26 (NA)
    %     0x0 ;       % 27 (NA)
    %     0x0 ;       % 28 (NA)
    %     0x0 ;       % 29 (NA)
    %     0x0 ;       % 30 (NA)
    %     0x48000000; % 31
        });

    if n_register > length(taps_list)
        error("Maximum order allowed for PRBS is %d.\n", length(taps_list));
    end

    % If no taps set, return error
    if taps_list(n_register) == 0
        error("PRBS for order %d is not defined.\n", n_register);
    end
    
    %% Check initial u
    if size(u_init, 1) ~= n_register
        logger("ERROR", "Number of rows of initialization must be %d!", n_register);
        return
    end
    
    % Convert to binary 0 and 1
    u_init = double(u_init > 0.5);
    
    %% PRBS Computation
    period = 2^n_register - 1;
    taps = find(dec2bin(taps_list(n_register)) == '1');

    u = ones(period*nu, nu) * 0.5;
    for iu = 1:nu
        tmp_u = zeros(period, 1);
        tmp_u(1:n_register) = u_init(:, iu);
        for i = n_register+1:period
            for j = taps
                tmp_u(i) = xor(tmp_u(i), tmp_u(i-n_register-1+j));
            end
        end
        idx = (iu-1)*period + (1:period);
        u(idx, iu) = tmp_u;
    end

    u = repmat(u, n_periods, 1);
    u = 2 * (u - 0.5);

end