%% Generate random models and perform uncertainty set identification
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
script_exp;

%% Plot results and save them in `Plots` folder
mkdir("Plots");
plot_figures_exp;

%% Post Clean-up
clearvars; clear("yalmip");
close all; clc;