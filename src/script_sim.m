%% Script for Simulation
% 
%   Performs the following actions:
%   1. Generate random model realizations for active suspension systems.
%   2. Uncertainty set identification (elliptical & disk) for the FRFs.
% 
%   Saves a `.mat` file for plot generation.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

% Cleanup before starting
clearvars;
clear('yalmip');
close all;
clc;

%% Basic config
n_FRF = 10;     % []
Fs = 400;       % [Hz]
Ts = 1 / Fs;    % [s]

solver = "mosek"; % One of: mosek or sedumi

% Set a random seed for repeatibility (Can be removed!)
rng(0);

%% Active suspension systems

% Physical parameters
mb = ureal('mb', 3.0e2, 'Percentage', 10);  % kg
mw = ureal('mw', 6.0e1, 'Percentage', 10);  % kg
bs = ureal('bs', 1.0e3, 'Percentage', 10);	% N/m/s
ks = ureal('ks', 1.6e4, 'Percentage', 10);  % N/m
kt = ureal('kt', 1.9e5, 'Percentage', 10);  % N/m

% State matrices
A = [...
    [0  , 1  , 0     , 0    ]       ; 
    [-ks, -bs, ks    , bs   ] / mb  ;
    [0  , 0  , 0     , 1    ]       ;
    [ks , bs , -ks-kt, -bs  ] / mw  ;
    ];
B = [...
    [0 , 0   ]      ;
    [0 , 1e3 ] / mb	;
    [0 , 0   ]      ;
    [kt, -1e3] / mw];
C = [...
    1, 0,  0, 0 ;
    1, 0, -1, 0 ;
    A(2, :)     ;
    ];
D = [
    0 , 0 ;
    0 , 0 ;
    B(2,:);
    ];

ucar = ss(A, B, C, D);
ucar.StateName = {...
    'body travel (m)'   ;
    'body vel (m/s)'    ;
    'wheel travel (m)'  ;
    'wheel vel (m/s)'   ;
    };
ucar.InputName = {
    'r' ;   % Road displacement (disturbance)
    'fs';   % Force from active suspension (control input)
    };
ucar.OutputName = {...
    'xb';   % Body travel
    'sd';   % Suspension deflection
    'ab';   % Body acceleration
    };

% For SISO
ucar = ucar('ab', 'fs');

% Generate models & convert to dicrete time
cars = usample(ucar, n_FRF);
cars = c2d(cars, Ts);

%% Generate FRFs 
n_prbs = 15;
p_prbs = 10;
l_prbs = (2^n_prbs - 1) * p_prbs;
t = 0:Ts:Ts*(l_prbs-1);

frd_models = cell(1, n_FRF);
for i_sample = 1:n_FRF
    u = utils.prbs(n_prbs, p_prbs);

    c_y = lsim(cars(:, :, i_sample), u, t);
    
    Z = detrend(iddata(c_y, u, Ts, "Period", 2^n_prbs-1));
    frd_models{i_sample} = frd(etfe(Z));
end
frd_models = cat(3, frd_models{:});

%% Uncertainty set identification
w = logspace(...
    log10(frd_models.Frequency(1)), ...
    log10(frd_models.Frequency(end)), ...
    500).';

model_elliptical = find_elliptical_uncertainty(frd_models, w, "solver", solver);
model_disk = find_disk_uncertainty(frd_models, w, "solver", solver);

%% Save `.mat` file for plot generation
save(fullfile("Data", "sim_models.mat"))