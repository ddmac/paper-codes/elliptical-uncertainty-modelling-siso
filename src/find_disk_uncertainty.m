function model = find_disk_uncertainty(frd_models, w, options)
%FIND_DISK_UNCERTAINTY Find the disk uncertainty set [Hindi et al. (2002)]
% 
%   model = FIND_DISK_UNCERTAINTY(frd_models) returns a structure containing the
%       nominal model and the uncertainty set. Here, `frd_models` is the array 
%       of the FRF models under consideration.
% 
%   model = FIND_DISK_UNCERTAINTY(frd_models, w) returns a structure containing 
%       the nominal model and the uncertainty set computed at `w` (in [rad/s]).
% 
%   model = FIND_DISK_UNCERTAINTY(..., "G", G) returns a structure with the
%       nominal model set as `G`.
% 
%   model = FIND_DISK_UNCERTAINTY(..., "solver", solver) sets the solver to use 
%       for convex optimisation. Tested solvers are: Mosek (default) and SeDuMi.
% 
%   model = FIND_DISK_UNCERTAINTY(..., "verbosity", level) sets the verbosity
%       level.
% 
%   ----------------------------------------------------------------------------
%   Output:
% 
%     model
%       ├ NominalModel          % Estimated/fixed nominal model
%       ├ UncertaintySet
%       │  ├ Type = "disk"      % Type of uncertainty set
%       │  ├ A                  % Cell array of matrix A for the set
%       │  ├ W1                 % 3D array of weighting W1 for the set
%       │  ├ W2                 % 3D array of weighting W2 for the set
%       │  ├ Area               % Area of uncertainty
%       │  └ Frequencies        % Freq. at which set is computed [rad/s]
%       └ EllapsedTime          % Time taken
% 
% 
%   ----------------------------------------------------------------------------
%   Verbosity Levels:
%       0: No output
%       1: Basic logs
%       2: Complete logs with optimisation logs
% 
%   ----------------------------------------------------------------------------
%   Description:
%       Implements the method to find the disk uncertainty set for LTI systems
%       using the method proposed by Hindi et al. (2002).
%       
%           G(jω) = { Ĝ(jω) + W₁∆W₂ ⏐ ║∆║₂ ≤ 1 }
% 
%       Ĝ(jω) = Nominal Model
%       ∆     = Uncertainty
%       W₁,W₂ = Weighting filters
% 
% 
%       References:
%       [1] H. Hindi, C.-Y. Seong, and S. Boyd, “Computing optimal uncertainty
%           models from frequency domain data,” in Proceedings of the 41st IEEE 
%           Conference on Decision and Control, Dec. 2002, vol. 3, pp.
%           2898–2905. doi: 10.1109/CDC.2002.1184290.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

    arguments
        frd_models (:, :, :) lti
        w (:, 1) double = []
        options.G lti = tf();
        options.solver (1, 1) string = "mosek"
        options.verbosity (1, 1) int32 {mustBeMember(options.verbosity, [0, 1, 2])} = 1
    end
    
    % In case of error, return empty struct
    model = struct();
    
    % Logger handle
    logger = @(type, varargin) utils.logger_verbose(options.verbosity, type, varargin{:});
    logger("START", mfilename);
    cleanUp = onCleanup(@() logger("STOP", mfilename));
    
    [ny, nu, N] = size(frd_models);

    if isempty(w)
        % If freq. points are not specified
        w = logspace(...
                log10(frd_models.Frequency(1)), ...
                log10(frd_models.Frequency(end)), ...
                500).';
        logger("DEBUG", "Using 500 logarithmically spaced frequency points for set")
    end
    n_w = length(w);
    
    Ts = frd_models.Ts;
    eta = 1e-6;
       
    %% Ellipsoidal uncertainty set & Nominal model   
    tic;
    
    frd_models_f = freqresp(frd_models, w);

    % Optimisation variables for Step 1
    T1 = sdpvar(ny, ny, n_w, 'hermitian', 'complex');
    T2 = sdpvar(nu, nu, n_w, 'hermitian', 'complex');
    t  = sdpvar(1, 1, n_w);
    
    if isempty(options.G)
        logger("INFO", "Finding disk uncertainty set and the nominal model...");
        G = sdpvar(ny, nu, n_w, 'full', 'complex');
    else
        logger("INFO", "Finding disk uncertainty set with fixed nominal model...");
        G = freqresp(options.G, w);
    end
    
    % Additional optimisation variables for Step 2
    S0 = sdpvar(nu, nu, n_w);
    S1 = sdpvar(ny, ny, n_w);
    S2 = sdpvar(nu, nu, n_w);
    
    % Step 1: Intial solve for t* ----------------------------------------------
    logger("INFO", "Step 1: Solving for optimal t ...");

    constraints = [...
        T1 <= t
        T2 <= t
        ];
    for i_w = 1:n_w
        for i = 1:N
            err = frd_models_f(:, :, i_w, i) - G(:, :, i_w);
            constraints = [constraints;
                [...
                    T1(:, :, i_w), err;
                    err'   , T2(:, :, i_w);
                ] >=0;
                ];
        end
    end
    objective = sum(t);

    opt = sdpsettings( ...
        'solver', char(options.solver), ...
        'verbose', options.verbosity==2 ...
        ); 
    JOB = optimize(constraints, objective, opt);

    if JOB.problem ~= 0
        logger("ERROR", "Optimisation failed!");
        return
    end
    t_opt = double(t);
    logger("INFO", "Step 1: Found optimal t");

    % Step 2: Solve for eta-suboptimal G0, T1, T2 ------------------------------
    logger("INFO", "Step 2: Solving for eta-suboptimal G0, T1, and T2 (eta=%.0e)...", eta);

    constraints = [...
        T1 <= (1 + eta) * t_opt * eye(ny);
        T2 <= (1 + eta) * t_opt * eye(nu);
        ];
    objective = 0;
    for i_w = 1:n_w
        for i = 1:N
            err = frd_models_f(:, :, i_w, i) - G(:, :, i_w);
            constraints = [constraints;
                [...
                    T1(:, :, i_w), err;
                    err'   , T2(:, :, i_w);
                ] >=0;
                ];
        end
        constraints = [constraints;
            [...
                eye(ny)         , G(:, :, i_w);
                G(:, :, i_w)'  , S0(:, :, i_w);
            ] >=0;
            [...
                eye(ny)         , T1(:, :, i_w);
                T1(:, :, i_w)' 	, S1(:, :, i_w);
            ] >=0;
            [...
                eye(nu)         , T2(:, :, i_w);
                T2(:, :, i_w)'  , S2(:, :, i_w);
            ] >=0;
            ];
        objective = objective + trace(S0(:, :, i_w)) + trace(S1(:, :, i_w)) + trace(S2(:, :, i_w));
    end

    opt = sdpsettings( ...
        'solver', char(options.solver), ...
        'verbose', options.verbosity==2 ...
        ); 
    JOB = optimize(constraints, objective, opt);

    if JOB.problem ~= 0
        logger("ERROR", "Optimisation failed!");
        return
    end
    logger("INFO", "Step 2: Found eta-suboptimal G0, T1, and T2");
    
    % Extract model ------------------------------------------------------------
    G = frd(double(G), w, Ts);
    T1 = double(T1);
    T2 = double(T2);

    r = zeros(n_w, 1);
    W1 = zeros(size(T1));
    W2 = zeros(size(T2));
    for i_w = 1:n_w
        r(i_w) = sqrt(norm(T1(:, :, i_w), 2) * norm(T2(:, :, i_w), 2));

        W1(:, :, i_w) = sqrtm(T1(:, :, i_w));
        W2(:, :, i_w) = sqrtm(T2(:, :, i_w)');
    end
    A = arrayfun(@(x) eye(2) * (1./x), r, "UniformOutput", false);
    
    ellapsed_time = toc;
    logger("INFO", "Disk uncertainty set found in %.4f s", ellapsed_time)

    %% Output
    model = struct(...
        "NominalModel", G, ...
        "UncertaintySet", struct(...
            "Type"       , "disk", ...
            "A"          , {A}, ...
            "W1"         , W1, ...
            "W2"         , W2, ...
            "Area"       , pi * r.^2, ...
            "Frequencies", w ...
        ), ...
        "EllapsedTime", ellapsed_time ...
        );

end