# Elliptical uncertainty modelling - SISO
<!-- Badges -->
[![License: MIT][license-badge]](/LICENSE)
[![Maintainter: Vaibhav Gupta][maintainer-badge]](mailto:vaibhav.gupta@epfl.ch)
[![DOI][DOI-badge]][DOI_link]  

This repository contains the code that has been used for

```bibtex
@article{gupta_DatadrivenIQCBasedUncertainty_2023,
  title   = {Data-Driven {{IQC-Based Uncertainty Modelling}} for {{Robust Control Design}}},
  author  = {Gupta, Vaibhav and Klauser, Elias and Karimi, Alireza},
  year    = {2023},
  journal = {IFAC-PapersOnLine},
  series  = {22nd {{IFAC World Congress}}},
  volume  = {56},
  number  = {2},
  pages   = {4789--4795},
  doi     = {10.1016/j.ifacol.2023.10.1244}
}
```

<!----------------------------------------------------------------------------->
## Requirements

- **MATLAB** (2019b or newer)
  - Control System Toolbox
  - Signal Processing Toolbox
  - Statistics and Machine Learning Toolbox
- **Toolbox manager** (`tbxmanager`) for MATLAB
  - yalmip
  - sedumi
- **MOSEK** version 9.2 or above

### Installation Instructions

- **`tbxmanager`**
  - Run [`utils.install_tbxmanager()`](src/+utils/install_tbxmanager.m) to install `tbxmanager` with all required submodules.
- **MOSEK**
  - Download appropiate installer from [MOSEK][mosek-download] website and install the software.
  - For academic uses, a [free license][mosek-academic] could be requested. For commercial purposes, a 30-day [trial license][mosek-commercial] is also available.

<!----------------------------------------------------------------------------->
## Repository structure

The structure of the `src` folder in the repository is as follows:

```shell
  ┣ +datadriven                       # Modified datadriven controller design package
  ┣ +utils                            # Utility functions
  ┣ Data                              # Folder for generated/used data
  ┃   ┣ PRBS_13                       # Exp. data for PRBS with register length 13
  ┃   ┣ sq_waveform_exp               # Exp. data for square waveform ref tracking
  ┃   ┃ ...
  ┣ Plots                             # Folder for generated plots
  ┣ design_controller.m               # Function for controller synthesis 
  ┣ find_disk_uncertainty.m           # Function for disk uncertainty
  ┣ find_elliptical_uncertainty.m     # Function for elliptical uncertainty
  ┣ get_margin.m                      # Function for stability margin estimation
  ┣ plot_figures_exp.m                # Script for plotting exp. data
  ┣ plot_figures_sim.m                # Script for plotting simulation data
  ┣ script_exp.m                      # Script for exp. uncertainty estimation and controller design
  ┣ script_sim.m                      # Script for simulation uncertainty estimation
  ┣ main_SimulationExample.m          # Script for "Simulation Example" section of the paper
  ┗ main_ExperimentalResults.m        # Script for "Experimental Results" section of the paper
```

<!----------------------------------------------------------------------------->
## Usage Instructions

Set the current folder to the `src` folder.

- To generate the data and plots from the section "Simulation Example", run `main_SimulationExample.m`.
- To generate the data and plots from the section "Experimental Results", run `main_ExperimentalResults.m`.

<!----------------------------------------------------------------------------->
<!-- Markdown Links -->
[license-badge]: https://img.shields.io/gitlab/license/ddmac/paper-codes/elliptical-uncertainty-modelling-siso?label=License&gitlab_url=https%3A%2F%2Fgitlab.epfl.ch
[maintainer-badge]: https://img.shields.io/badge/Maintainer-Vaibhav_Gupta-blue.svg
[DOI-badge]: https://img.shields.io/badge/DOI-10.1016/j.ifacol.2023.10.1244-blue.svg
[DOI_link]: https://doi.org/10.1016/j.ifacol.2023.10.1244
[mosek-download]: https://www.mosek.com/downloads/
[mosek-academic]: https://www.mosek.com/products/academic-licenses/
[mosek-commercial]: https://www.mosek.com/products/trial/
