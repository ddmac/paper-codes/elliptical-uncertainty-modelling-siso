%% Plots for paper (Experiment)
% 
%   Generate plots for paper from the experiment data.
% 
%   Loads `.mat` file geenrated during controller synthesis, and `.bin` files
%   generatede during square waveform reference following experiments.
% 
%   ----------------------------------------------------------------------------
%   Copyright 2022 Vaibhav Gupta, DDMAC, EPFL (MIT License)
% 

% Cleanup before starting
clearvars;
clear('yalmip');
close all;
clc;

%% Basic config

% Export Graphic Options (set [] for paper)
exportgraphics_opts = ["ContentType", "vector", "BackgroundColor", "none"];

colors = struct(...
    "FRFs", ["#abcd5e", "#29ac9f", "#14976b", "#2b67af", "#fc8405", "#f9d531"], ...
    "reference", "#2b67af", ...
    "Nominal", "#050505", ...
    "EllipticalSet", "#0072BD", ...
    "DiskSet", "#D95319", ...
    "MeasSet", "#EDB120");

%% Load files
% Load `.mat` file
load(fullfile("Data", "exp_controller_synthesis.mat"));

% Load `.bin` files
n_samples = 2e3 * 5;

sq_waveform_files = dir("Data/sq_waveform_exp/logs_*.bin");
n_exp = length(sq_waveform_files);

y_sq_waveform = zeros(n_samples, n_exp);
u_sq_waveform = zeros(n_samples, n_exp);
r_sq_waveform = zeros(n_samples, n_exp);
for i_file = 1:n_exp
    [y, r, u, ~, anyRef, ~] = utils.read_binary_file(...
        fullfile(sq_waveform_files(i_file).folder, sq_waveform_files(i_file).name) ...
    );
    y_sq_waveform(:, i_file) = y(anyRef, 1);
    u_sq_waveform(:, i_file) = u(anyRef, 1);
    r_sq_waveform(:, i_file) = r(anyRef, 1);
end

%% Plot `bodemag` of FRFs and the nominal model for elliptical uncertainty set
mag_G = abs(squeeze(freqresp(frd_models, w)));

[mag, ~, ~] = bode(model_elliptical.NominalModel, w);
mag_G_hat = squeeze(mag);

fig = figure('Position', [0, 0, 500, 250]);
tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile(); 
hold on; box on; grid minor; grid on;

ax.TickLabelInterpreter = "latex";
ax.FontSize = 14;
ax.Box = "on";
ax.XScale = 'log';

for idx = 1:n_FRF
    h_meas = plot(w, mag2db(mag_G(:, idx)), "Color", colors.FRFs(idx), 'LineWidth', 0.75);
end
h_est = plot(w, mag2db(mag_G_hat), "Color", colors.Nominal, 'LineWidth', 1.5);

xlim([w(1), w(end)]); ylim([-60, 30]);

leg = legend(h_est, ...
    "Estimated Nominal Model ($\hat{G}$)", ...
    "Location", "southwest"); 
leg.Interpreter = "latex";
leg.FontSize = 14;
leg.Box = "off";

ylabel(ax, "Magnitude [dB]", "Interpreter", "latex");
xlabel(ax, "Frequency [rad/s]", "Interpreter", "latex");

fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "exp_freq_plot.pdf"), exportgraphics_opts{:});

%% Plot elliptical and disk uncertainty sets at some freq. points
i_ws = [400, 135, 80, 10];

z_frd_models = squeeze(freqresp(frd_models, w));
z_elliptical = squeeze(freqresp(model_elliptical.NominalModel, w));
z_disk = squeeze(freqresp(model_disk.NominalModel, w));

fig = figure("Position", [0, 0, 500, 250]);
tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile(); 
hold on; axis equal; box on; grid minor; grid on;

ytickformat('$%d$'); xtickformat('$%d$');
ax.TickLabelInterpreter = "latex";
ax.FontSize = 14;
ax.Box = "on";

text_x = [ 1,  3, 4.5,  14];
text_y = [-1, -3, -11, -12];
mkstys = ["d", "o", "s", "h"];
mksize_meas = 8;
mksize = 10;
for idx = 1:length(i_ws)
    i_w = i_ws(idx);
    mksty = mkstys(idx);

    % Measurements
    h_meas = plot(real(z_frd_models(i_w, :)), imag(z_frd_models(i_w, :)), ...
        "LineStyle", "none", ...
        "Marker", mksty, ...
        "MarkerSize", mksize_meas, ...
        "Color", colors.MeasSet, ...
        "MarkerFaceColor", colors.MeasSet);

    % Disk Uncertainty Set
    [h_disk, ~] = utils.plot_ellipse(...
        z_disk(i_w), model_disk.UncertaintySet.A{i_w},...
        "Marker", mksty, ...
        "MarkerSize", mksize, ...
        "Color", colors.DiskSet);

    % Elliptical Uncertainty Set
    [h_elliptical, ~] = utils.plot_ellipse(...
        z_elliptical(i_w), model_elliptical.UncertaintySet.A{i_w},...
        "Marker", mksty, ...
        "MarkerSize", mksize, ...
        "Color", colors.EllipticalSet);

    % Frequency labels
    text(text_x(idx), text_y(idx), ...
        sprintf("$%.2f$ [rad/s]", w(i_w)), ...
        "FontSize", 14, ...
        "Interpreter", "latex");
end

leg = legend([h_meas, h_disk, h_elliptical], ...
    "Measurements ($G_i$)", ...
    "Hindi et al. (2002)", ...
    "Proposed Method", ...
    "Location", "northeast");
leg.Interpreter = "latex";
leg.FontSize = 14;
leg.Box = "off";

xlabel(ax, "Real Axis", "Interpreter", "latex");
ylabel(ax, "Imaginary Axis", "Interpreter", "latex");

fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "exp_comp_plot_nominal_models.pdf"), exportgraphics_opts{:});

%% Plot Step
% Compute output of the reference model
t = (0:Ts:(n_samples-1)*Ts);
y_nominal = lsim(M_LS, r_sq_waveform(:, 1), t);

% Move start to some mid point
t_start = 15.5;
t = t - t_start;

% Indexes of measurements known apriori
knownIdx = [1, 3, 6, 7, 10, 12];

fig = figure('Position', [0, 0, 500, 250]);
tiledlayout(1, 1, "TileSpacing", "compact", "Padding", "compact");
ax = nexttile();
hold on; box on; grid minor; grid on;

ax.TickLabelInterpreter = "latex";
ax.FontSize = 14;
ax.Box = "on";

h_ref = plot(t, r_sq_waveform(:, 1), ...
    "Color", colors.reference, ...
    "LineStyle", "--", ...
    "LineWidth", 1.5);
for idx = 1:6
    plot(t, y_sq_waveform(:, knownIdx(idx)), ...
        "Color", colors.FRFs(idx), ...
        "LineStyle", "-", ...
        "LineWidth", 0.75);
end
h_nominal = plot(t, y_nominal, ...
    "Color", colors.Nominal, ...
    "LineStyle", "--", ...
    "LineWidth", 1.5);

ylabel(ax, "Magnitude [rad/s]", "Interpreter", "latex");
xlabel(ax, "Time [s]", "Interpreter", "latex");

xlim([0, 4]); ylim([-3, 3]);

leg = legend([h_ref, h_nominal], ...
    "Reference Signal", ...
    "Reference Model", ...
    "Location", "south"); 
leg.Position = leg.Position - [0.1, 0, 0, 0];
leg.Interpreter = "latex";
leg.FontSize = 14;
leg.Box = "off";

fig.Units = "centimeters";
fig.PaperSize = fig.Position(3:4);
fig.PaperUnits = "normalized";
fig.PaperPosition = [0, 0, 1, 1];

exportgraphics(fig, fullfile("Plots", "exp_step_plot.pdf"), exportgraphics_opts{:});
